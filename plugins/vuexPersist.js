import createPersistedState from 'vuex-persistedstate'

export default ({ store }) => {
  createPersistedState({
    paths: ['history'],
    key: 'calculator',
    getState: (key, storage) => {
      const persitData = JSON.parse(storage.getItem(key))
      // const storeState = store.state
      // If something come from SSR, remove it.
      // if (storeState.user.data) {
      //   persitData && persitData.user && delete persitData.user.data
      // }
      return persitData
    },
    storage: localStorage
  })(store)
}
