export const state = () => ({
  data: false
})

export const mutations = {
  start (state) {
    state.data = true
  },
  end (state) {
    state.data = false
  }
}
