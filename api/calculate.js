import axios from './instance'

export default {
  calculate: (equation) => {
    const uriEncrypt = encodeURIComponent(equation)
    console.log(uriEncrypt)
    return axios.api.get(`/?expr=${uriEncrypt}`).then(response => response.data)
  }
}
